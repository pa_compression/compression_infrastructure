resource "aws_s3_bucket" "app" {
  bucket = "${var.prefix}-app"
  acl    = "private"

  tags = {
    Name        = "Compression bucket"
    Environment = "Dev"
  }
}