# Compression app infrastructure

## Description
The compression app infrastructure will be deploy as service into two EC2 instance

## Input data
| Name | Description |
|:---:| --- |
|**prefix**| Prefix for all your service |
|**source_bucket**| Bucket where aws code deploy will get the source code |
|**source_bucket_arn**| ARN of the bucket where the source code are|
|**source_bucket_file**| Source code zip in source bucket |

## Main created services

| Name | Quantity | Description
|:---:| :---: | --- |
|**ec2**| 2 | VM where compression app will be run |
|**code pipeline**| 1 | Pipeline to get your source code and push to code deploy |
|**code deploy**| 1 | Deploy new soure code to ec2 instance |
|**sqs**| 1 | Queue where the compression app will get the new request
|**s3**| 1 | Bucket where compression app will push result
