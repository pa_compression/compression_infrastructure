resource "aws_s3_bucket" "codepipeline_bucket" {
  bucket = "${var.prefix}-codepipeline"
  acl    = "private"
}

resource "aws_iam_role" "codepipeline_role" {
  name = "${var.prefix}-codepipeline"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "codepipeline.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOF
}

resource "aws_iam_role_policy" "codepipeline_policy" {
  role = aws_iam_role.codepipeline_role.id

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect":"Allow",
      "Action": [
        "s3:GetObject",
        "s3:GetObjectVersion",
        "s3:GetBucketVersioning",
        "s3:PutObject"
      ],
      "Resource": [
        "${aws_s3_bucket.codepipeline_bucket.arn}",
        "${aws_s3_bucket.codepipeline_bucket.arn}/*",
        "${var.source_bucket_arn}",
        "${var.source_bucket_arn}/*"
      ]
    },
    {
        "Action": [
            "codedeploy:CreateDeployment",
            "codedeploy:GetApplication",
            "codedeploy:GetApplicationRevision",
            "codedeploy:GetDeployment",
            "codedeploy:GetDeploymentConfig",
            "codedeploy:RegisterApplicationRevision"
        ],
        "Resource": "*",
        "Effect": "Allow"
    }
  ]
}
EOF
}

resource "aws_codepipeline" "codepipeline" {
  name     = var.prefix
  role_arn = aws_iam_role.codepipeline_role.arn

  artifact_store {
    location = aws_s3_bucket.codepipeline_bucket.bucket
    type     = "S3"
  }

  stage {
    name = "Source"

    action {
      name             = "Source"
      category         = "Source"
      owner            = "AWS"
      provider         = "S3"
      version          = "1"
      output_artifacts = ["source_output"]
      
      configuration = {
        S3Bucket: var.source_bucket
        S3ObjectKey: var.source_bucket_file
      }
    }
  }

  stage {
    name = "Deploy"

    action {
      name            = "Deploy"
      category        = "Deploy"
      owner           = "AWS"
      provider        = "CodeDeploy"
      input_artifacts = ["source_output"]
      version         = "1"

      configuration = {
        ApplicationName = var.prefix
        DeploymentGroupName = var.prefix
      }
    }
  }

  depends_on = [aws_codedeploy_deployment_group.app]
}

resource "aws_iam_role" "app_codedeploy" {
  name = "${var.prefix}-codedeploy"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "",
      "Effect": "Allow",
      "Principal": {
        "Service": "codedeploy.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "app_codedeploy" {
  role= aws_iam_role.app_codedeploy.id
  policy_arn= "arn:aws:iam::aws:policy/service-role/AWSCodeDeployRole"
}

resource "aws_codedeploy_app" "app_codedeploy" {
  name             = var.prefix
}

resource "aws_codedeploy_deployment_group" "app" {
  app_name               = "${aws_codedeploy_app.app_codedeploy.name}"
  deployment_group_name  = var.prefix
  service_role_arn       = "${aws_iam_role.app_codedeploy.arn}"

  ec2_tag_filter {
    key   = "Name"
    type  = "KEY_AND_VALUE"
    value = "${var.prefix}-app-instance"
  }
}