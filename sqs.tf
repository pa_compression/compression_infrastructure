resource "aws_sqs_queue" "waiting" {
    name = "${var.prefix}-waiting-task"
    redrive_policy = jsonencode({
        deadLetterTargetArn = aws_sqs_queue.dead.arn
        maxReceiveCount     = 3
    })
}


resource "aws_sqs_queue" "dead" {
    name = "${var.prefix}-dead-waiting-task"
}