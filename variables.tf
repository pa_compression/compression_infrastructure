variable "prefix" {
  description = "Prefix name for all ressources"
  default="esgi-pa"
}

variable "source_bucket" {
  description= "Bucket where aws codedeploy will get the source code" 
  default= "esgi-pa-deploy"
}

variable "source_bucket_file" {
  description= "Source code file in bucket" 
  default= "app.zip"
}

variable "source_bucket_arn" {
  description= "Arn of your bucket" 
  default= "arn:aws:s3:::esgi-pa-deploy"
}
