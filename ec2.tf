data "aws_ami" "ec2-app" {
  most_recent = true
  owners      = ["amazon"]
  filter {
    name   = "owner-alias"
    values = ["amazon"]
  }

  filter {
    name   = "name"
    values = ["amzn2-ami-hvm*"]
  }
  filter {
    name   = "architecture"
    values = ["x86_64"]
  }
}

resource "aws_iam_role" "ec2-app" {
  name = "${var.prefix}-app-instance-policy"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF

}

resource "aws_iam_role_policy" "ec2-app" {
  role = aws_iam_role.ec2-app.name
  policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect":"Allow",
      "Action": [
        "s3:Get*",
        "s3:List*",
        "s3:Put*"
      ],
      "Resource": ["*"]
    }
  ]
}
POLICY
}

resource "aws_iam_role_policy_attachment" "ec2-app" {
  role= aws_iam_role.ec2-app.name
  policy_arn= "arn:aws:iam::aws:policy/AmazonSQSFullAccess"
}


resource "aws_iam_instance_profile" "ec2-app" {
  name = "${var.prefix}-app-instance-profile"
  role = aws_iam_role.ec2-app.name
}

resource "aws_security_group" "ec2-app" {
  name        = "${var.prefix}-app-instance"

  ingress {
    protocol    = "tcp"
    from_port   = 22
    to_port     = 22
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    protocol    = "-1"
    from_port   = 0
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_instance" "ec2-app" {
  count=2
  ami                         = data.aws_ami.ec2-app.id
  instance_type               = "t2.micro"
  associate_public_ip_address = "true"
  source_dest_check           = "false"
  security_groups = [aws_security_group.ec2-app.name]
  iam_instance_profile        = aws_iam_instance_profile.ec2-app.id
  key_name = "esgi-pa" 
  user_data                   = <<EOF
#!/bin/bash
sudo yum -y update
sudo yum -y install wget ruby python3.7 git
wget https://aws-codedeploy-eu-west-3.s3.eu-west-3.amazonaws.com/latest/install -P /home/ec2-user
wget https://bootstrap.pypa.io/get-pip.py -P /home/ec2-user
chmod +x  /home/ec2-user/install
sudo  /home/ec2-user/install auto
python3 /home/ec2-user/get-pip.py
sudo sh -c 'echo "echo "hello-bash"" >> /tmp/env.sh'
sudo sh -c 'echo "export COMPRESSION_BUCKET_RESULT=${var.prefix}-app" >> /tmp/env.sh'
sudo sh -c 'echo "export COMPRESSION_WAITING_QUEUE=${var.prefix}-waiting-task" >> /tmp/env.sh'
chmod +x  /tmp/env.sh
  EOF

  tags = {
    Name = "${var.prefix}-app-instance"
  }
}